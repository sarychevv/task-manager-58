package ru.t1.sarychevv.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public final class JmsLoggerProducer {

    //@NotNull
    //private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String URL = System.getenv().getOrDefault("BROKER_URL", ActiveMQConnection.DEFAULT_BROKER_URL);

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer messageProducer;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final IPropertyService propertyService;

    @SneakyThrows
    public JmsLoggerProducer(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(propertyService.getJmsQueue());
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @SneakyThrows
    public void send(final OperationEvent operationEvent) {
        executorService.submit(() -> sync(operationEvent));
    }

    @SneakyThrows
    private void sync(@NotNull final OperationEvent operationEvent) {
        final Class<?> entityClass = operationEvent.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            operationEvent.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(operationEvent));
    }

}
