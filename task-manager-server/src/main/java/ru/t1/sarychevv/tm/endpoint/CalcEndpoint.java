package ru.t1.sarychevv.tm.endpoint;

import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.ICalcEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ICalcEndpoint")
public class CalcEndpoint extends AbstractEndpoint implements ICalcEndpoint {

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

}
