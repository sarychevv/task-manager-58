package ru.t1.sarychevv.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.sarychevv.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    public AbstractUserOwnedDTORepository(@NotNull final Class<M> type) {
        super(type);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public boolean existsById(@NotNull final String userId,
                              @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId,
                           @Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getTableName());
        @NotNull final Root<M> root = criteriaQuery.from(getTableName());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId,
                         @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId,
                            @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final String userId,
                       @NotNull final M model) throws Exception {
        entityManager.remove(model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId,
                       @NotNull final M model) throws Exception {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}

