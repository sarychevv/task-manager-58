package ru.t1.sarychevv.tm.component;

import org.jetbrains.annotations.NotNull;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.sarychevv.tm.listener.EntityListener;

import javax.jms.*;

public final class Bootstrap {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @SneakyThrows
    public void init() {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
