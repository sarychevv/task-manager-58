package ru.t1.sarychevv.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.sarychevv.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

}
