package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class TaskChangeStatusByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-id";
    }

    @Override
    @EventListener(condition = "@TaskChangeStatusByIdListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setStatus(status);
        request.setId(id);
        getTaskEndpoint().changeTaskStatusById(request);

    }

}
