package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.listener.AbstractListener;

@Component
public class ArgumentListListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@ArgumentListListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener.getArgument() != null && !listener.getArgument().isEmpty())
                System.out.println(listener.getArgument());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show argument list.";
    }

}
