package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.ILoggerService;
import ru.t1.sarychevv.tm.event.ConsoleEvent;

@Component
public class ApplicationExitListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@ApplicationExitListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ILoggerService loggerService = getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application";
    }

}
