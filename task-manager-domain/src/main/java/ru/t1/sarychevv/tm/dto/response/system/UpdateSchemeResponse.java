package ru.t1.sarychevv.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

import javax.xml.bind.annotation.XmlType;

@NoArgsConstructor
@Getter
@Setter
@XmlType(name = "UpdateScheme")
public final class UpdateSchemeResponse extends AbstractResultResponse {

    public UpdateSchemeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
